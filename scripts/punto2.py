#!/usr/bin/env python3

############################## Sección de importaciones #######################################
import rospy
from geometry_msgs.msg import Twist
from std_msgs.msg import Float32MultiArray, Float32
#from tkinter import *
import threading
import matplotlib.pyplot as plt
from matplotlib import animation
import os
import argparse
import copy
import numpy as np

global Vel_ang_der
global Vel_ang_izq
global X_inercial
global Y_inercial
global X_odom
global Y_odom
global z_local
global theta
global time
global dt
time = 0
dt = 50e-3 ## Medida en segundos
X_inercial = []
Y_inercial = []
X_odom = []
Y_odom = []
theta = []

def callbackTime(data):
	global time
	time = data.data

def callbackGraph(data):	
	global X_inercial, Y_inercial, X_odom, Y_odom, Vel_ang_der, Vel_ang_izq, z_local, dt, theta


	X_inercial.append(data.linear.x)
	Y_inercial.append(data.linear.y)

	if len(X_odom) < 2:
		X_odom.append(X_inercial[-1])
		Y_odom.append(Y_inercial[-1])
		z_local = (data.angular.y - np.pi/2)*-1
		#theta.append(data.angular.y -np.pi)

	
	
	try:
		vel_lineal = (0.035/2)*(Vel_ang_der+Vel_ang_izq)
		vel_ang = -(0.035/0.115)*(Vel_ang_der-Vel_ang_izq)
		if Vel_ang_izq != Vel_ang_der:
			R = abs(vel_lineal/vel_ang)
		else:
			R = 999999
	
	
	
		Iccx = X_odom[-1] + R*np.cos(z_local-(vel_ang/abs(vel_ang))*np.pi/2)
		Iccy = Y_odom[-1] + R*np.sin(z_local-(vel_ang/abs(vel_ang))*np.pi/2)
		zeta = np.arctan2(Y_odom[-1]-Iccy, X_odom[-1]-Iccx)
		theta1 = vel_ang*dt 
		X_odom.append(R*np.cos(zeta+theta1)+Iccx)
		Y_odom.append(R*np.sin(zeta+theta1)+Iccy)
		z_local = z_local + theta1
		# X_odom.append(X_odom[-1] + vel_lineal * np.cos(theta[-1]) * dt)
		# Y_odom.append(Y_odom[-1] + vel_lineal * np.sin(theta[-1]) * dt)
		# theta.append(theta[-1] + vel_ang*dt )
		
	except:
		pass
	
	



def set_graph():
	Dynamic = animation.FuncAnimation(plt.figure(1), animar, 1000000)
	plt.show()

def animar(i):
	global X_inercial, Y_inercial
	plt.cla()
	plt.plot(X_inercial, Y_inercial, label = 'Real')
	plt.plot(X_odom,Y_odom, label='Odom')
	plt.axis([-2.5,2.5,-2.5,2.5])
	plt.title('Posición Turtle_Bot')
	plt.ylabel('y [m]')
	plt.xlabel('x [m]')
	plt.legend()

def main(nombre_archivo):
	global time, Vel_ang_der, Vel_ang_izq, dt

	rospy.init_node('Generar_movimiento', anonymous = True)

	pub = rospy.Publisher('turtlebot_wheelsVel', Float32MultiArray, queue_size = 10)
	rospy.Subscriber('simulationTime', Float32, callbackTime)
	rospy.Subscriber('turtlebot_position', Twist, callbackGraph)
	rate = rospy.Rate(40)

	wheel_vel = Float32MultiArray()
	wheel_vel.data = [0,0]

	file1 = open(str(os.path.abspath('')) + '/src/taller_2_solucion/resources/' + nombre_archivo + ".txt","r")
	perfiles = file1.readlines()
	No_simulaciones = perfiles[0][0]

	grapher = threading.Thread(target = set_graph)
	grapher.start()

	while time == 0:
		pass

	current_time = copy.copy(time)
	time_event = []

	print('Perfil de velocidad acual: ',perfiles[1])

	cto = 1
	i = 0
	avance = 1
	while not rospy.is_shutdown():
		
		Vel_ang_der =  float(perfiles[cto].split()[0])
		Vel_ang_izq =  float(perfiles[cto].split()[1])
		time_to_simulate = float(perfiles[cto].split()[2])

		wheel_vel.data = [Vel_ang_der,Vel_ang_izq]
		pub.publish(wheel_vel)
		rate.sleep()

		#time_event.append(float(time))
		#if i == 10: 
		#	dt = time_event[-1]-time_event[-2]
		#	avance = 0
		#i +=avance
		if cto == int(No_simulaciones): 
			plt.gcf()
			plt.savefig(str(os.path.abspath('')) + '/src/taller2_00/results/' + 'figura.png' )
			rospy.loginfo('Figura Guardada')
			break
			
		if time-current_time >= time_to_simulate : 
			cto += 1
			current_time = copy.deepcopy(time)
			print('Perfil de velocidad acual: ',perfiles[cto])
		


################################## Función principal  #######################################
if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('Nombre_archivo', type = str, help = 'Ingrese Nombre del archivo (Sin incluir .txt)' )
	args = parser.parse_args()
	main(args.Nombre_archivo)