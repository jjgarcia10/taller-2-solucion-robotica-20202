#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

laserdata = np.loadtxt('laserscan.dat')
angle_min = -np.pi/2
angle_max = np.pi/2
num_data = len(laserdata)
theta =  np.linspace(angle_min, angle_max, num_data)

plt.figure(1)
plt.scatter(theta, laserdata)
plt.xlabel('Theta [Rad]')
plt.ylabel('Distancia de medición [m]')
plt.title('Datos marco local en rectangulares')

x = laserdata*np.cos(theta+np.pi/4) + 1 + 0.2*np.cos(np.pi/4)
y = laserdata*np.sin(theta+np.pi/4) + 0.5 + 0.2*np.sin(np.pi/4)
plt.figure(4,figsize=(4,4))
plt.scatter(x,y )
plt.plot(0.230*np.cos(np.linspace(0,2*np.pi,100)) + 1, 0.230*np.sin(np.linspace(0,2*np.pi,100)) + 0.5)
plt.xlabel('Theta [Rad]')
plt.ylabel('Distancia de medición [m]')
plt.title('Datos marco inercia en rectangulares')
plt.axis('equal')
plt.show()
