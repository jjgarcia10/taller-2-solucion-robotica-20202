#!/usr/bin/env python3
import rospy
import sys, os
import copy 
from geometry_msgs.msg import Twist 
from std_msgs.msg import Float32, Float32MultiArray
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

radii = 0.035
l = 0.23

simtime = 0
pos_x = 0
pos_y = 0
pos_theta = None
theta = 0

def simTime_callback(msg):
	global simtime
	simtime = msg.data

def orientation_callback(msg):
	global theta
	theta = msg.data	

def position_callback(msg):
	global pos_x, pos_y, pos_theta
	pos_x = msg.linear.x
	pos_y = msg.linear.y
	pos_theta = msg.angular.z	

endPos = [1.0, 1.0, np.pi/4]
rospy.init_node('control_turtlebot2', anonymous=True)
myargv = rospy.myargv(argv=sys.argv)
if len(myargv)>2:
	endPos[0] = myargv[1]
	endPos[1] = myargv[2]
	endPos[2] = myargv[3]

velocity_publisher = rospy.Publisher('/turtlebot_wheelsVel', Float32MultiArray, queue_size=10)
rate = rospy.Rate(10)
rospy.Subscriber("simulationTime", Float32, simTime_callback)
rospy.Subscriber("turtlebot_position", Twist, position_callback)
rospy.Subscriber("turtlebot_orientation", Float32, orientation_callback)

vel_msg = Float32MultiArray()
vel_msg.data = [0.0, 0.0]


vrep_move_x = []
vrep_move_y = []
vrep_theta = []
errorPos_x = []
errorPos_y = [] 
errorTheta = []
t = []
plotTime = []

vrep_move_x.append(pos_x)
vrep_move_y.append(pos_y)
vrep_theta.append(theta)
errorPos_x.append(endPos[0] - vrep_move_x[-1])
errorPos_y.append(endPos[1] - vrep_move_y[-1])
errorTheta.append(endPos[2] - theta)
plotTime.append(simtime)
t.append(0)


rho = np.sqrt(endPos[0]**2 + endPos[1]**2)
alpha = -theta + np.arctan2(endPos[1], endPos[0])
beta = errorTheta[-1]


K_rho = 0.25 #0.25
K_alpha = 0.5 #0.5
K_beta = 0.0

while not rospy.is_shutdown():
	velX_frameR = 0
	velW_frameR = 0
	vel_msg.data = [0.0, 0.0]
	velocity_publisher.publish(vel_msg)

	# while simtime<30 and rho>0.05:
	while simtime < 10:
		t.append(simtime)
		deltaT = simtime - t[-2]
		if deltaT!=0:
			plotTime.append(simtime)
			vrep_move_x.append(pos_x)
			vrep_move_y.append(pos_y)
			vrep_theta.append(theta)
			

			deltaX = endPos[0] - vrep_move_x[-1]
			deltaY = endPos[1] - vrep_move_y[-1]
			eTheta = endPos[2] - theta
			rho = np.sqrt(deltaX**2 + deltaY**2)
			alpha = -theta + np.arctan2(deltaY, deltaX)
			beta = eTheta
			
			errorPos_x.append(deltaX)
			errorPos_y.append(deltaY)
			errorTheta.append(eTheta)

			velX_frameR = K_rho * rho
			if np.abs(velX_frameR) > 7.0:
				velX_frameR = 7.0*np.sign(velX_frameR)

			velW_frameR = K_alpha*alpha + K_beta*beta
			if np.abs(velW_frameR) > np.pi:
				velW_frameR = np.pi*np.sign(velW_frameR)

			if alpha>=-np.pi/2 and alpha<=np.pi/2:
				print("X="+str(velX_frameR)+"  W="+str(velW_frameR)+"  possitive alpha")
				vr = (velX_frameR + velW_frameR*(l/2))/radii
				vl = (velX_frameR - velW_frameR*(l/2))/radii
			elif (alpha>-np.pi and alpha<=-np.pi/2) or (alpha>np.pi/2 and alpha<=np.pi):				
				print("X="+str(velX_frameR)+"  W="+str(velW_frameR)+"  negative alpha")
				vr = (-velX_frameR + velW_frameR*(l/2))/radii
				vl = (-velX_frameR - velW_frameR*(l/2))/radii
			vel_msg.data = [vl, vr]
			velocity_publisher.publish(vel_msg)			
			print( str(theta/np.pi) + ' ' + str(rho) + ' ' + str(eTheta) )
			rate.sleep()

	vel_msg.data = [0.0, 0.0]
	velocity_publisher.publish(vel_msg)
	break


#TODO: control de orientacion
plt.figure(1)
plt.subplot(2, 1, 1)
plt.plot(np.array(vrep_move_x), np.array(vrep_move_y))
plt.xlabel('VREP x')
plt.ylabel('VREP y')
plt.subplot(2, 1, 2)
plt.plot(np.array(plotTime), vrep_theta)
plt.ylabel('vrep_theta')

plt.figure(2)
plt.subplot(3, 1, 1)
plt.plot(np.array(plotTime), np.array(errorPos_x))
plt.ylabel('Error position in x')
plt.subplot(3, 1, 2)
plt.plot(np.array(plotTime), np.array(errorPos_y))
plt.ylabel('Error position in y')
plt.subplot(3, 1, 3)
plt.plot(np.array(plotTime), np.array(errorTheta))
plt.ylabel('Error orientation')
plt.xlabel('simulation time')
plt.show()


	